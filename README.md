Back up FTP sites hosted at .MIL and .GOV sites with [ftp-gov-grab](https://github.com/ArchiveTeam/ftp-gov-grab)
as part of the [government backup](http://archiveteam.org/index.php?title=Government_Backup).

# Volumes
- `/var/lib/grabber/data`

# Environment Variables
## CONCURRENT
How many threads run at the same time.

## NICKNAME
Nickname that you want to be shown as, on the tracker.

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
