FROM quay.io/thallian/alpine-s6:latest

RUN addgroup -g 2222 grabber
RUN adduser -h /var/lib/grabber -u 2222 -D -G grabber grabber

RUN apk add --no-cache libressl python3 rsync gcc python3-dev musl-dev linux-headers
RUN pip3 install seesaw requests psutil wpull html5lib==0.9999999 https://github.com/thallian/warc/archive/master.tar.gz

RUN wget -qO- https://github.com/thallian/ftp-gov-grab/archive/master.tar.gz | tar -xz -C /var/lib/grabber --strip 1

RUN chown -R grabber:grabber /var/lib/grabber

RUN apk del gcc python3-dev musl-dev linux-headers

ADD /rootfs /

VOLUME /var/lib/grabber/data
